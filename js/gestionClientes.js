var clientesObtenidos;

function getClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if (this.readyState == 4 && this.status == 200){
      //console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }

  request.open("GET",url, true);
  request.send();
}

function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);

  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var divTabla = document.getElementById("divTablaClientes");
  var tabla = document.createElement("table");
  var tBody = document.createElement("tBody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre  = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCity  = document.createElement("td");
    columnaCity.innerText = JSONClientes.value[i].City;

    var columnaBandera  = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if (JSONClientes.value[i].Country == "UK") {
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    }else{
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }

    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCity);
    nuevaFila.appendChild(columnaBandera);

    tBody.appendChild(nuevaFila);
  }

  tabla.appendChild(tBody);
  divTabla.appendChild(tabla);
}
